resource "aws_route53_zone" "domain_name" {
  name    = "${var.application}-${var.env}.internal."
  comment = "${var.application} ${var.env} internal domain"

  tags {
    Application = "${var.application}"
    Environment = "${var.env}"
  }
}

output "private_dns_zone_id" {
  value = "${aws_route53_zone.domain_name.id}"
}
