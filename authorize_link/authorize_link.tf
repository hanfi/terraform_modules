resource "aws_security_group_rule" "link" {
  type                     = "${var.type}"
  from_port                = "${var.port}"
  to_port                  = "${var.port}"
  protocol                 = "${var.protocol}"
  source_security_group_id = "${var.source_security_group_id}"
  security_group_id        = "${var.destination_security_group_id}"
}
