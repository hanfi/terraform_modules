variable "type" {
  default = "ingress"
}

variable "port" {}

variable "protocol" {
  default = "tcp"
}

variable "source_security_group_id" {}
variable "destination_security_group_id" {}
