data "aws_subnet_ids" "subnets" {
  vpc_id = "${var.vpc_id}"

  tags = {
    Service = "${var.service}"
  }
}

######################## Security Group ALB ####################################

resource "aws_security_group" "alb-security-group" {
  name        = "${var.env}-${var.application}-${var.service}-sgp-alb"
  description = "SG ALB ${var.env}-${var.application}-${var.service}"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = "${var.alb_port}"
    to_port     = "${var.alb_port}"
    cidr_blocks = ["${var.cidr_block}"]
    protocol    = "tcp"
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "${var.env}-${var.application}-${var.service}-sgp-alb"
    Environment = "${var.env}"
    Application = "${var.application}"
    Service     = "${var.service}"
  }
}

######################## External ALB ##########################################

resource "aws_alb" "alb" {
  name                       = "${var.env}-${var.application}-${var.service}-alb"
  subnets                    = ["${data.aws_subnet_ids.subnets.ids}"]
  security_groups            = ["${aws_security_group.alb-security-group.id}"]
  internal                   = false
  enable_deletion_protection = false

  tags {
    Name        = "${var.env}-${var.application}-${var.service}-alb"
    Environment = "${var.env}"
    Application = "${var.application}"
    Service     = "${var.service}"
  }
}

######################## ALB Target groups #####################################

resource "aws_alb_target_group" "alb-target-group" {
  name     = "${var.env}-${var.application}-${var.service}-alb-target"
  port     = "${var.exposed_instances_port}"
  protocol = "${var.exposed_instances_protocol}"
  vpc_id   = "${var.vpc_id}"

  health_check {
    healthy_threshold   = "2"
    unhealthy_threshold = "2"
    timeout             = "2"
    interval            = "5"
    path                = "/status"
  }

  tags {
    Name        = "${var.env}-${var.application}-${var.service}-alb-target-group"
    Environment = "${var.env}"
    Application = "${var.application}"
    Service     = "${var.service}"
  }
}

######################## ALB Listeners #########################################

resource "aws_alb_listener" "alb-listner" {
  load_balancer_arn = "${aws_alb.alb.arn}"
  port              = "${var.alb_port}"
  protocol          = "${var.alb_protocol}"

  default_action {
    target_group_arn = "${aws_alb_target_group.alb-target-group.arn}"
    type             = "forward"
  }
}

################## INTERNAL CNAME for ALB ######################################

resource "aws_route53_record" "app_alb_private_alias" {
  zone_id = "${var.dns_zone_id}"
  name    = "${var.service}"
  type    = "${var.alb_private_alias_record_type}"
  ttl     = "${var.alb_private_alias_ttl}"
  records = ["${aws_alb.alb.dns_name}"]
}
