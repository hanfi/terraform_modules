variable "env" {}
variable "application" {}
variable "service" {}
variable "vpc_id" {}
variable "alb_port" {}
variable "release_version" {}

variable "alb_protocol" {
  default = "HTTP"
}

variable "cidr_block" {}

variable "exposed_instances_port" {
  default = "80"
}

variable "exposed_instances_protocol" {
  default = "HTTP"
}

variable "dns_zone_id" {}

variable "alb_private_alias_record_type" {
  default = "CNAME"
}

variable "alb_private_alias_ttl" {
  default = "300"
}

variable "instance_type" {}
variable "user_data" {}
variable "key_name" {}

variable "security_groups" {
  default = []
}

variable "asg_min" {}
variable "asg_max" {}
variable "asg_desired" {}

variable "health_check_grace_period" {
  default = 300
}

variable "health_check_type" {
  default = "ELB"
}

variable "wait_for_capacity_timeout" {
  default = "10m"
}
