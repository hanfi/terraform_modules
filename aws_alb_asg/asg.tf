####################### Get Availability Zones ##############################

data "aws_availability_zones" "availability_zones" {}

######################## Get AMI ID #########################################

data "aws_ami" "ami-id" {
  most_recent      = true

  filter {
    name   = "tag:Application"
    values = ["${var.application}"]
  }

  filter {
    name   = "tag:Service"
    values = ["${var.service}"]
  }

  filter {
    name   = "tag:Version"
    values = ["${var.release_version}"]
  }

  name_regex = "^${var.application}-${var.service}-${var.release_version}"
  owners     = ["self"]
}

######################## IAM Instance Profile ##################################

# IAM Instance Profile
resource "aws_iam_instance_profile" "instance-profile" {
  name = "${var.env}-${var.application}-${var.service}-instance-profile"
  role = "${aws_iam_role.iam-role.name}"
}

resource "aws_iam_role" "iam-role" {
  name = "${var.env}-${var.application}-${var.service}-iam-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

######################## Security Group instances ##########################

resource "aws_security_group" "instance-security-group" {
  name        = "${var.env}-${var.application}-${var.service}-instance-sgp"
  description = "${var.env}-${var.application}-${var.service}"
  vpc_id      = "${var.vpc_id}"

  tags {
    Name        = "${var.env}-${var.application}-${var.service}-instance-sgp"
    Environment = "${var.env}"
    Application = "${var.application}"
    Service     = "${var.service}"
  }
}

resource "aws_security_group_rule" "allow_egress" {
  type              = "egress"
  from_port         = "0"
  to_port           = "0"
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.instance-security-group.id}"
}

resource "aws_security_group_rule" "allow_from_alb" {
  type                     = "ingress"
  from_port                = "${var.exposed_instances_port}"
  to_port                  = "${var.exposed_instances_port}"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.instance-security-group.id}"
  source_security_group_id = "${aws_security_group.alb-security-group.id}"
}

######################## ASG Launch Conf. ##################################

resource "aws_launch_configuration" "launch_configuration" {
  image_id      = "${data.aws_ami.ami-id.id}"
  name_prefix   = "${var.env}-${var.application}-${var.service}-${var.release_version}-lc"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"

  security_groups = [
    "${aws_security_group.instance-security-group.id}",
    "${var.security_groups}",
  ]

  user_data            = "${var.user_data}"
  iam_instance_profile = "${aws_iam_instance_profile.instance-profile.name}"

  lifecycle {
    create_before_destroy = true
  }
}

######################## ASG  ###############################################

resource "aws_autoscaling_group" "autoscaling-group" {
  name                      = "${var.env}-${var.application}-${var.service}-${var.release_version}-${data.aws_ami.ami-id.id}-asg"
  launch_configuration      = "${aws_launch_configuration.launch_configuration.name}"
  availability_zones        = ["${data.aws_availability_zones.availability_zones.names}"]
  vpc_zone_identifier       = ["${data.aws_subnet_ids.subnets.ids}"]
  target_group_arns         = ["${aws_alb_target_group.alb-target-group.arn}"]
  wait_for_capacity_timeout = "${var.wait_for_capacity_timeout}"
  health_check_type         = "${var.health_check_type}"
  health_check_grace_period = "${var.health_check_grace_period}"
  min_size                  = "${var.asg_min}"
  max_size                  = "${var.asg_max}"
  wait_for_elb_capacity     = "${var.asg_desired}"
  desired_capacity          = "${var.asg_desired}"

  lifecycle {
    create_before_destroy = true
  }

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  /* Common tags */
  tag {
    key                 = "AMI"
    value               = "${data.aws_ami.ami-id.id}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Name"
    value               = "${var.env}-${var.application}-${var.service}-${var.release_version}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Service"
    value               = "${var.service}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Environment"
    value               = "${var.env}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Application"
    value               = "${var.application}"
    propagate_at_launch = "true"
  }

  tag {
    key                 = "Version"
    value               = "${var.release_version}"
    propagate_at_launch = "true"
  }
}

output "security_group" {
  value = "${aws_security_group.instance-security-group.id}"
}
