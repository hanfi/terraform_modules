variable "env" {}
variable "application" {}
variable "service" {}
variable "vpc_id" {}
variable "db_engine" {}
variable "db_engine_version" {}
variable "db_instance_class" {}
variable "db_multi_az" {}

variable "db_storage_type" {
  default = "gp2"
}

variable "db_allocated_storage" {}
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "db_parameter_group" {}

variable "db_snapshot_identifier" {
  default = ""
}

variable "private_host_zone" {}

variable "record_type" {
  default = "CNAME"
}

variable "ttl" {
  default = "300"
}

variable "skip_final_snapshot" {
  default = "true"
}
