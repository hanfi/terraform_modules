data "aws_subnet_ids" "subnets" {
  vpc_id = "${var.vpc_id}"

  tags = {
    Service = "${var.service}"
  }
}

############################## Security Group ##################################

# Create Security Group for DB
resource "aws_security_group" "database-sg" {
  name        = "${var.env}-${var.application}-${var.service}-rds-sgp"
  description = "${var.env}-${var.application}-${var.service}"
  vpc_id      = "${var.vpc_id}"

  tags {
    Name        = "${var.env}-${var.application}-${var.service}-instance-sgp"
    Applcation  = "${var.application}"
    Service     = "${var.service}"
    Environment = "${var.env}"
  }
}

############################# Subnet Groups ####################################
resource "aws_db_subnet_group" "subnet-group" {
  name       = "${var.env}-${var.application}-${var.service}-subnet-group"
  subnet_ids = ["${data.aws_subnet_ids.subnets.ids}"]

  tags {
    Name        = "${var.env}-${var.application}-${var.service}-subnet-group"
    Applcation  = "${var.application}"
    Service     = "${var.service}"
    Environment = "${var.env}"
  }
}

############################# RDS ##############################################

# Create RDS instance
resource "aws_db_instance" "database" {
  identifier                  = "${var.db_snapshot_identifier == "" ? "${var.env}-${var.application}-${var.service}-${var.db_engine}" : ""}"
  engine                      = "${var.db_engine}"
  engine_version              = "${var.db_engine_version}"
  instance_class              = "${var.db_instance_class}"
  multi_az                    = "${var.db_multi_az}"
  storage_type                = "${var.db_storage_type}"
  allocated_storage           = "${var.db_allocated_storage}"
  name                        = "${var.db_name}"
  username                    = "${var.db_username}"
  password                    = "${var.db_password}"
  vpc_security_group_ids      = ["${aws_security_group.database-sg.id}"]
  parameter_group_name        = "${var.db_parameter_group}"
  db_subnet_group_name        = "${var.env}-${var.application}-${var.service}-subnet-group"
  allow_major_version_upgrade = true
  snapshot_identifier         = "${var.db_snapshot_identifier}"
  copy_tags_to_snapshot       = true
  skip_final_snapshot         = "${var.skip_final_snapshot}"

  tags {
    Name           = "${var.env}-${var.application}-${var.service}-${var.db_engine}"
    Service        = "${var.service}"
    Environment    = "${var.env}"
    Application    = "${var.application}"
    Engine         = "${var.db_engine}"
    Engine_version = "${var.db_engine_version}"
  }
}

######################### Route53 Records #####################################

resource "aws_route53_record" "alias_db" {
  zone_id = "${var.private_host_zone}"
  name    = "${var.application}-${var.service}-db"
  type    = "${var.record_type}"
  ttl     = "${var.ttl}"
  records = ["${aws_db_instance.database.address}"]
}

###################### Parameter Store DB params ##############################
resource "aws_ssm_parameter" "host" {
  name  = "/${var.env}/${var.application}/${var.service}/host"
  type  = "String"
  value = "${aws_db_instance.database.address}"
}
resource "aws_ssm_parameter" "name" {
  name  = "/${var.env}/${var.application}/${var.service}/name"
  type  = "String"
  value = "${aws_db_instance.database.name}"
}
resource "aws_ssm_parameter" "port" {
  name  = "/${var.env}/${var.application}/${var.service}/port"
  type  = "String"
  value = "${aws_db_instance.database.port}"
}
resource "aws_ssm_parameter" "username" {
  name  = "/${var.env}/${var.application}/${var.service}/username"
  type  = "String"
  value = "${aws_db_instance.database.username}"
}
resource "aws_ssm_parameter" "password" {
  name  = "/${var.env}/${var.application}/${var.service}/password"
  type  = "SecureString"
  value = "${var.db_password}"
}

#################### Module Outputs ############################################

output "host" {
  value = "${aws_db_instance.database.address}"
}

output "name" {
  value = "${aws_db_instance.database.name}"
}

output "port" {
  value = "${aws_db_instance.database.port}"
}

output "username" {
  value = "${aws_db_instance.database.username}"
}

output "password" {
  value = "${var.db_password}"
}

output "fqdn" {
  value = "${aws_route53_record.alias_db.fqdn}"
}

output "security_group" {
  value = "${aws_security_group.database-sg.id}"
}
