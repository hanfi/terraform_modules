resource "aws_ssm_parameter" "secret" {
  name  = "${var.env}/${var.application}/${var.service}/${var.version}/${var.key}"
  type  = "${var.type}"
  value = "${var.value}"
}