variable "env" {}
variable "application" {}
variable "service" {}
variable "version" {}
variable "key" {}
variable "value" {}
variable "type" { default = "String"}